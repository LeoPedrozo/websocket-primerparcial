# websocket-primerparcial
- Basado en el codigo del tp con una modificacion en el texto del cliente.
- Alumno: Oscar Pedrozo
- ci: 4472036

# codigo original del tp
- https://gitlab.com/larrea.pedrolarrea.pedro/websockethome.git

# Requerimientos
- Java EE 8: jdk8u301-windows-x64.exe Descarga Directa : https://drive.google.com/file/d/1XeMTMktYZDYmuwy2LyEkUaVKi1dSUOXG/view?usp=sharing
- Glassfish 4.1.1 Disponible en : https://javaee.github.io/glassfish/download
